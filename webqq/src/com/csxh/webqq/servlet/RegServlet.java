package com.csxh.webqq.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class RegServlet
 */
public class RegServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//获取浏览传过来的参数
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		
		//向浏览器回传响应数据（html）
		response.setCharacterEncoding("gbk");
		response.getWriter().print("<h2>");
		response.getWriter().print(username);
		response.getWriter().print("</h2>");
		response.getWriter().print("<h1>注册成功</h1>");
		response.getWriter().flush();
	}

}
