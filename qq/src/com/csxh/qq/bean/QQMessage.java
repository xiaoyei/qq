package com.csxh.qq.bean;

import java.util.Map;

import com.csxh.qq.util.StringUtil;

/**
 * 符合javabean规范的消息类
 * 
 * @author hp
 *
 */
public class QQMessage {

	public static final String QQ = "qq";
	public static final String USERFACE = "userface";
	public static final String USERNAME = "username";
	public static final String SEPT = "__";
	public static final String GROUP_SEPT = ":";
	public static final String PASSWORD = "pwd";
	
	private Long id;// 将当前时间截做为id值
	private QQMessageType type = QQMessageType.Text;
	private String title;
	private StringBuffer contents;// 该字段用于传递一些附加信息，如密码、用户名、图标名称，这些信息使用key=value形式格式化
	// 来源QQ号
	private String from;
	// 去向QQ号
	private String to;

	// 状态码：200表示成功，400客户端产生的错误,500服务端产生的错误
	private Integer status = 200;

	public QQMessage() {
		init();
	}

	// 初始化成员属性值
	public void init() {

		this.id = null;// 将当前时间截做为id值
		this.type = QQMessageType.Unkown;
		this.title = null;
		this.contents = new StringBuffer();
		// 来源QQ号
		this.from = null;
		// 去向QQ号
		this.to = null;
		// 状态码：200表示成功，400客户端产生的错误,500服务端产生的错误
		this.status = 0;
	}

	// 格式化用户密码，返回对象本身，以便进行连缀操作
	public QQMessage formatPassword(String password) {

		if (this.contents.length() != 0) {
			this.contents.append(GROUP_SEPT);
		}
		this.contents.append(PASSWORD).append(SEPT).append(password);

		return this;
	}

	public String parsePassword() {

		if (this.contents.length() != 0) {
			this.contents.append(GROUP_SEPT);
		}

		return parseValue(PASSWORD);

	}

	// 格式化用户名称，返回对象本身，以便进行连缀操作
	public QQMessage formatUserName(String username) {

		if (this.contents.length() != 0) {
			this.contents.append(GROUP_SEPT);
		}
		this.contents.append(USERNAME).append(SEPT).append(username);

		return this;
	}

	public String parseUserName() {

		if (this.contents.length() != 0) {
			this.contents.append(GROUP_SEPT);
		}

		return parseValue(USERNAME);

	}

	// 格式化用户图象名称，返回对象本身，以便进行连缀操作
	public QQMessage formatUserFace(String userface) {

		if (this.contents.length() != 0) {
			this.contents.append(GROUP_SEPT);
		}
		this.contents.append(USERFACE).append(SEPT).append(userface);

		return this;
	}

	public String parseUserFace() {

		if (this.contents.length() != 0) {
			this.contents.append(GROUP_SEPT);
		}

		return parseValue(USERFACE);

	}

	private String parseValue(String key) {
		// k1=v1&k2=v2
		String[] ss = this.contents.toString().trim().split(GROUP_SEPT);
		for (String s : ss) {
			// k1=v1
			String[] ss2 = s.split(SEPT);
			if (ss2.length == 2) {
				String field = ss2[0];// k1
				String value = ss2[1];// v1
				if (field.equals(key)) {
					return value;
				}

			}
		}
		return "";
	}

	// 将属性格式为key=value&key=value;
	public String formatParams() {

		if (this.type == QQMessageType.Unkown || StringUtil.isEmpty(this.from)) {
			return "";
		}

		StringBuffer sb = new StringBuffer();

		sb.append("type=").append(this.type).append("&");
		if (!StringUtil.isEmpty(this.title)) {
			sb.append("title=").append(this.title).append("&");
		}

		sb.append("contents=").append(this.contents).append("&");

		sb.append("from=").append(this.from).append("&");
		if (!StringUtil.isEmpty(this.to)) {
			sb.append("to=").append(this.to);
		}

		sb.append("status=").append(this.status);

		return sb.toString();
	}

	// 将格式为key=value&key=value的串解析为属性
	public boolean parseParams(String params) {

		init();

		if (StringUtil.isEmpty(params)) {
			return false;
		}
		// k1=v1&k2=v2
		String[] ss = params.trim().split("&");
		for (String s : ss) {
			// k1=v1
			String[] ss2 = s.split("=");
			if (ss2.length == 2) {
				String field = ss2[0];// k1
				String value = ss2[1];// v1
				set(field, value);
			}
		}
		return true;
	}

	public boolean parseParams(Map<String, Object> params) {
		init();
		for (Map.Entry<String, Object> entry : params.entrySet()) {
			String field = entry.getKey();
			String value = entry.getValue().toString();
			set(field, value);
		}
		return false;
	}

	private void set(String field, String value) {
		if (field.equals("type")) {
			// 根据枚举对象的值，转成枚举类型的对象
			this.type = Enum.valueOf(QQMessageType.class, value);
		} else if (field.equals("title")) {
			this.title = value;
		} else if (field.equals("contents")) {
			this.contents.append(value);
		} else if (field.equals("from")) {
			this.from = value;
		} else if (field.equals("to")) {
			this.to = value;
		} else if (field.equals("status")) {
			try {
				this.status = Integer.parseInt(value);
			} catch (NumberFormatException e) {
				this.status = 400;
			}
		}
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public QQMessageType getType() {
		return type;
	}

	public void setType(QQMessageType type) {
		this.type = type;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContents() {
		return contents.toString();
	}

	public void setContents(String contents) {
		this.contents.append(contents);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QQMessage other = (QQMessage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Message [id=" + id + ", type=" + type + ", title=" + title + ", contents=" + contents + "]";
	}

}
