package com.csxh.qq.net;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class NetRequest {

	private Socket client;

	private String urlPath=Net.unkown;
	private Map<String, Object> params=new HashMap<>();//key=value条目
	
	public NetRequest(Socket client) {
		super();
		this.client = client;
	}

	
	//解析客户请求的命令与参数
	public void parse() throws Exception{
		
		//首先读取协议的头部
		String url=Net.receiveRequestUrl(this.client);
		this.urlPath=Net.parseRequestUrl(url, this.params);
		
	}

	public String getUrlPath(){
		return this.urlPath;
	}
	
	public Map<String, Object> getParametersMap(){
		
		return this.params;
		
	}
	
	public String getServletName(ServletConfig config){
		
		return config.getServletName(this.urlPath);
		
	}
	
	public Object getObjectParameter(String key){
		return this.params.get(key);
	}
	
	public String getStringParameter(String key){
		return this.params.get(key).toString();
	}
	
	public Integer getIntParameter(String key){
		return (Integer)this.params.get(key);
	}
	
	
	//获取字节输入流
	private InputStream getInputStream() throws IOException {
		// TODO Auto-generated method stub
		return this.client.getInputStream();
	}
    //获取字符输入流
	private BufferedReader getBufferedReader() throws IOException {
		// TODO Auto-generated method stub
		return new BufferedReader(new InputStreamReader(getInputStream(), Net.charset));
	}


	public void initClient() throws IOException{
		// TODO Auto-generated method stub
		
	}
	
}
