package com.csxh.qq.net;

import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import com.csxh.qq.bean.QQMessage;
import com.csxh.qq.bean.QQMessageType;
import com.csxh.qq.ui.QQDialog;
import com.csxh.qq.util.StringUtil;

/**
 * 使用Socket开发客户端程序
 * 
 * @author hp
 *
 */
public class NetClient extends Net {

	public static void main(String[] args) throws Exception {
		// TODO Auto-generated method stub

		// 连接到指定IP和port的服务程序，并通信请求
		Socket client = new Socket(Net.host, Net.port);
		
		//向服务器发出QQ注册请求
		//将QQ通信协议封装在一个类QQMessage中
		String qq="123456";
		String password="1234";
		//实际开发中密码要进行加密处理
	    QQMessage message=new QQMessage();
		//设置消息类型是注册
	    message.setType(QQMessageType.Reg);
	    message.setFrom(qq);//设置QQ号
	    message.formatPassword(password);//设置密码
	    message.formatUserName(StringUtil.randomAlphabetString(4, 6));//随机产生一个，应该从用户控件中获取
	    message.formatUserFace("face1.jpg");//指定一个，应该从用户控件中获取

	    
	    String params=message.formatParams();//将属性格式为key=value&key=value;
	    
		String url=Net.buildRequestUrl(Net.host, Net.port, Net.qqMessage, params);

		//JOptionPane.showMessageDialog(null, url);
		
		Net.sendRequestUrl(client, url);
		
		//等待服务回答
		String data= Net.receiveResponseData(client);
		params=data;
		
		//关闭socket
		client.close();

		
	}

}
