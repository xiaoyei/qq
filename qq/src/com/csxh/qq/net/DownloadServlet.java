package com.csxh.qq.net;

import java.io.IOException;
import java.util.Map;

public class DownloadServlet extends Servlet {

	public DownloadServlet(NetRequest request, NetResponse response,ServletConfig config) {
		super(request, response, config);
		// TODO Auto-generated constructor stub
	}
	
	
	@Override
	protected void service(NetRequest request, NetResponse response) throws IOException {
		//用于进行文伯下载处理
		//不写实际代码，只是向客户端发送请求的参数
		String path=request.getUrlPath();
		Map<String, Object> params=request.getParametersMap();
		String host=(String)params.get("host");
		String url=Net.buildResponseUrl(host);
		String data="xxxx";
		Net.sendResponseData(response.getPrintWriter(),url,data);
		
	}
	
}
