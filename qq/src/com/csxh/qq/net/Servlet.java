package com.csxh.qq.net;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

public abstract class Servlet implements Runnable{

	private NetRequest request;
	private NetResponse response;
	private ServletConfig config;
	public Servlet(NetRequest request,NetResponse response, ServletConfig config) {
		super();
		this.request=request;
		this.response=response;
		this.config=config;
	}

	@Override
	public void run() {
		
		
		try {
			//在使用服务之前初始化方法
			init(this.request, this.config);
			//在服务方法方法
			service(this.request,this.response);
			//在使用服务之后销毁方法
			destroy(this.response);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	protected void init(NetRequest request, ServletConfig config) throws IOException {
		// TODO Auto-generated method stub
		request.initClient();
	}

	//在此进行请求与响应的处理
	protected abstract void service(NetRequest request, NetResponse response) throws IOException;


	protected void destroy(NetResponse response) throws IOException {
		
		response.closeClient();
		
	}

}