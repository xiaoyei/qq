package com.csxh.qq.net;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ServletConfig {

	private Properties conf;
	public ServletConfig() throws IOException {
		
		File file=new File("servlet.properties");
		InputStream in=new FileInputStream(file);
		this.conf = new Properties();
		this.conf.load(in);
		in.close();
		
	}
	//根据请求命令的名称，获取处理这个命令的servlet的全限定名称
	public String getServletName(String cmd){
		
		return this.conf.getProperty(cmd);
	}
}
