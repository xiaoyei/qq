package com.csxh.qq.net;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class NetResponse {

	private Socket client;
	
	public NetResponse(Socket client) {
		// TODO Auto-generated constructor stub
		this.client=client;
	}
	

	//获取字节输出流，用于向客户端传递图片、音频数据
	public OutputStream getOutputStream() throws IOException {
		// TODO Auto-generated method stub
		return this.client.getOutputStream();
	}

	//获取字符打印流，用于向客户端传格式化的文本数据
	public PrintWriter getPrintWriter() throws IOException{
		// TODO Auto-generated method stub
		return new PrintWriter(new OutputStreamWriter(getOutputStream(),Net.charset),true);
	}


	public void closeClient() throws IOException{
	//	this.client.close();
	}
	
}
