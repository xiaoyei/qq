package com.csxh.qq.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.csxh.qq.util.StringUtil;


public abstract class Net {

	// IP地址协议
	public static String host = "127.0.0.1";
	// 端口协议
	public static int port = 8888;
	// 编码协议
	public static String charset = "utf-8";
	// 请求协议（urlPath）
	public static String downloadFile = "/downloadFile";
	public static String uploadFile = "/uploadFile";
	public static String quit = "/quit";
	public static String unkown = "/xxx";

	//用于处理QQ通信的协议路径
	public static String qqMessage = "/qq";
    
	public static String protocolName = "http";
	// 协议的正则表达式格式
	public static String protocolFormat = "^(?<name>%s)(://)(?<host>\\w+(-\\w+)*(\\.\\w+(-\\w+)*)*)(?<port>:[\\d]+)?/(?<path>\\w+(/\\w+)*)*(?<params>\\?(\\w+=[\\w[^u4E00-u9FA5][:]]+[\\.\\-\\w+]*)(&\\w+=[\\w[^u4E00-u9FA5][:]]+[\\.\\-\\w+]*)*)*$";

	public static Map<String, Object> parseProtocol(String url) {

		Map<String, Object> map = new HashMap<>();

		String regex=String.format(protocolFormat, protocolName);
		Pattern p = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(url.trim());

		if (!m.find()) {
			throw new RuntimeException(url+"：协议格式不正确");
		}
		
		map.put("name", m.group("name"));
		map.put("host", m.group("host"));
		map.put("port", m.group("port"));
		String path = "/" + m.group("path");
		map.put("path", path);
		String params = m.group("params");
		if (StringUtil.isEmpty(params)) {
			map.put("params", "");
			return map;
		}
		// 除掉?号
		params = params.substring(1);
		// k1=v1 &k2=v2
		String[] ss = params.split("&");
		for (int i = 0; i < ss.length; i++) {
			String[] ss2 = ss[i].split("=");
			String key = ss2[0].trim();
			String value = ss2[1].trim();
			// 判断是否可以转换为数值
			try {
				Integer v = Integer.valueOf(value);
				map.put(key, v);
			} catch (NumberFormatException e) {
				try {
					Double f = Double.valueOf(value);
					map.put(key, f);
				} catch (NumberFormatException e1) {

					if (value.equalsIgnoreCase("true")) {
						map.put(key, true);
					} else if (value.equalsIgnoreCase("false")) {
						map.put(key, false);
					} else {
						map.put(key, value);
					}

				}
			}
		}

		return map;

	}

	
	// 构建请求协议的url
	public static String buildRequestUrl(String host, int port, String urlPath, String params) throws Exception {// params=k1=v1&k2=v2
		
		StringBuffer sb = new StringBuffer();
		sb.append(protocolName).append("://").append(host).append(":").append(port).append(urlPath).append("?")
				.append(params).append("\r\n");
	
		String url=sb.toString();
		
		return url;
	}

	// 发送请求方法
	public static void sendRequestUrl(Socket client, String url) throws Exception {
		    
		    OutputStream os = client.getOutputStream();
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os, Net.charset));
			bw.write(url);
			bw.flush();
			// 不能关闭输出流，否则会引起socket的关闭
 	}
	
	// 解析请求协议的path与params
	public static String parseRequestUrl(String url, Map<String, Object> params) throws Exception {
       Map<String, Object> map = parseProtocol(url);
		params.putAll(map);// 将内部的映射块复制到传入的映射块中
		return (String) params.get("path");
	}

	// 接收请求方法
	public static String receiveRequestUrl(Socket client) throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(client.getInputStream(), Net.charset));
		String ret = br.readLine();
		// 不能关闭输入流，否则会引起socket的关闭
		return ret;
	}

	public static void config() {
		String defaultProperties = "net.properties";
		try {

			InputStream is = Net.class.getClassLoader().getResourceAsStream(defaultProperties);
			if (is == null) {
				return;
			}
			Properties p = new Properties();
			p.load(is);
			host = p.getProperty("host", host).trim();
			String s = p.getProperty("port", "" + port).trim();
			port = Integer.valueOf(s);
			downloadFile = p.getProperty("downloadFile", downloadFile).trim();
			uploadFile = p.getProperty("uploadFile", uploadFile).trim();
			qqMessage = p.getProperty("qqMessage", qqMessage).trim();
			quit = p.getProperty("quit", quit).trim();

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static String buildResponseUrl(String host) {
		StringBuffer sb = new StringBuffer();
		sb.append(protocolName).append("://").append(host).append("\r\n");
		return sb.toString();
	}

	public static String parseResponseUrl(String host) {
		StringBuffer sb = new StringBuffer();
		sb.append(protocolName).append("://").append(host).append("\r\n");
		return sb.toString();
	}

	
	public static void sendResponseData(PrintWriter out,String url, String data)throws IOException {
		
		out.print(url);
		out.println();//多加一个回车换行，以区别url与data
		out.println(data);
		out.flush();
		
	}

	public static String receiveResponseData(Socket client) throws IOException {
		BufferedReader br=new BufferedReader(new InputStreamReader(client.getInputStream(), Net.charset));
		String url=br.readLine();//还要验证响应的协议的正确性及一些可选参数，一个重要的参数是数据的长度，数据的编码格式
		br.readLine();//空行
		String data=br.readLine();
		return data;
	}

}
