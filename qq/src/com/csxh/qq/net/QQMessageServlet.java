package com.csxh.qq.net;

import java.io.IOException;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.Map;

import javax.swing.JOptionPane;

import com.csxh.qq.bean.QQMessage;
import com.csxh.qq.service.QQUserService;

public class QQMessageServlet extends Servlet {

	public QQMessageServlet(NetRequest request, NetResponse response, ServletConfig config) {
		super(request, response, config);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void service(NetRequest request, NetResponse response) throws IOException {
		// 处理/qq协议
		// 提取将对应的QQMessage的属性
		QQMessage message = new QQMessage();
		Map<String, Object> params = request.getParametersMap();
		boolean b=message.parseParams(params);
		String qq=message.getFrom();//将QQ号当成用户名
		String password=message.parsePassword();//将QQ内容当成密码
		QQUserService service=new QQUserService();

		switch (message.getType()) {//判断接收的是什么类型的QQ消息
		case Text:

			break;
		case Login:
			//使用用户服类进行注册
			Map<String, String>map=service.login(qq, password);
			if (null!=map) {
				message.setStatus(200);//设置注册成功标识码
				//将密码、用户名和用户头象格式化到message的contents字段
				message.formatPassword(map.get(QQMessage.PASSWORD));
				message.formatUserName(map.get(QQMessage.USERNAME));
				message.formatUserFace(map.get(QQMessage.USERFACE));
				//用户好友列表信息也要格式化到message的contents字段
				
			}
			break;
		case Reg://注册
			
            //使用用户服类进行注册
			String username=message.parseUserName();
			String userface=message.parseUserFace();
			//System.out.println(username);
			b=service.reg(qq, password,username,userface);
			if (b) {
				message.setStatus(200);//设置注册成功标识码
			}
			
			break;

		default:
			break;
		}
		String host = (String) params.get("host");
		String url = Net.buildResponseUrl(host);
		String data =message.formatParams();//将属性格式化为：k1=v1&k2=v2的串
		Net.sendResponseData(response.getPrintWriter(), url, data);

	}

}
