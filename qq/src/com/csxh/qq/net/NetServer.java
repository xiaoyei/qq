package com.csxh.qq.net;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 使用ServerSocket实现网络通信服务器（服务端的程序）
 * 
 * @author hp
 *
 */
public class NetServer extends Net {

	// 主线程，则JVM启动
	public static void main(String[] args) throws Exception {

		ServerSocket server = new ServerSocket(Net.port);
		
		ServletConfig config=new ServletConfig();

		System.out.println("服务器在端口监听（值班）....");//显示到UI的界面中

		while (true) {
			
			Socket client = server.accept();// 在指定端口上等待客户的到来，如果有访问到达，就返回代表客户的Socket对象
	
			System.out.println("accept");
			
			//创建一个处理客户请求的线程对象，并启动
			NetRequest request = new NetRequest(client);
			NetResponse response = new NetResponse(client);
			//解析请求的命令（Action）与参数
			request.parse();
			
			//获取servlet的全限定名：带包的类名称
			String servletName=request.getServletName(config);
			
			//根据命令来派发servlet
			//通过反射根据全限定名称来创建servlet对象
			@SuppressWarnings("static-access")
			Class<?>clazz=NetServer.class.forName(servletName);
			Servlet servlet=(Servlet)clazz.getConstructor(request.getClass(),response.getClass(),config.getClass()).newInstance(request,response,config);
		   	
			new Thread(servlet).start();
		   	
			try{
				Thread.sleep(100);
			}catch(InterruptedException e){
				break;
			}
			
		}
		
		server.close();
		

	}

}
