package com.csxh.qq.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JOptionPane;

import com.csxh.qq.bean.QQMessage;
import com.csxh.qq.util.StringUtil;

/**
 * QQ用户服务类：完成注册、登录
 * 
 * @author Administrator
 *
 */
public class QQUserService {

	public boolean checkFormat(String qq, String password) {
		boolean b = StringUtil.checkFormat(qq, 6, 6, "\\d+");
		if (!b) {
			return false;
		}
		b = StringUtil.checkFormat(password, 4, 20, "\\w+");
		if (!b) {
			return false;
		}
		return true;
	}

	// 注册功能:qq号的长度6，密码长度为4以上
	public boolean reg(String qq, String password,String username,String userface) {
		// 验证输入内容的格式是否正确
		boolean b=checkFormat(qq, password);
		
		if (!b) {
			return b;
		}// 入库，请xx完成
		try {
			File f = new File("user.txt");
			if (!f.exists()) {
				f.createNewFile();
			}
			// 使用多线程安全串
			StringBuffer sb = new StringBuffer();
			// 对密码进行加密：由xx完成
			sb.append(qq).append(":")
			.append(password).append(":")
			.append(username).append(":")
			.append(userface).append("\r\n");
			FileWriter fw = new FileWriter(f,true);//在文件后添加
			fw.write(sb.toString());
            fw.close();
		} catch (IOException e) {
			return false;
		}

		return true;

	}

	
	public Map<String, String> login(String qq,String password){
		Map<String, String> map=null;
		boolean b=checkFormat(qq, password);
		if (!b) {
			return map;
		}
		try {
			File f = new File("user.txt");
			if (!f.exists()) {
				return map;
			}
			
			//sb.append(qq).append(":").append(password).append("\r\n");
			FileReader fw = new FileReader(f);
			BufferedReader br=new BufferedReader(fw);
			String user;
			b=false;
			while (  (user=br.readLine())!=null ) {
				String[] ss=user.split(":");
				String qq2=ss[0];
				// 对密码进行解密：由xx完成
				String password2=ss[1];
				
				if(qq.equals(qq2) && password.equals(password2)){
					//读取用户名与头象
					String username=ss[2];
					String userface=ss[3];
					map=new HashMap<>();
					map.put(QQMessage.QQ, qq2);
					map.put(QQMessage.PASSWORD, password2);
					map.put(QQMessage.USERNAME, username);
					map.put(QQMessage.USERFACE, userface);
					break;
				}
			}
			br.close();//该语句应该放在finally块中
		} catch (IOException e) {
			return null;
		}
		return map;
		
	}
	
}
