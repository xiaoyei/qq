package com.csxh.qq.ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.csxh.qq.bean.QQMessage;

import java.awt.GridBagLayout;
import javax.swing.JTable;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import javax.swing.JLabel;
import javax.swing.JTextPane;
import java.awt.Color;

public class QQListFrame extends JFrame {

	private JPanel contentPane;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					QQListFrame frame = new QQListFrame();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	QQMessage message;
	public QQListFrame(QQMessage message) {
		
		this.message=message;
		setLocationRelativeTo(null);//相对于桌面居中显示
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 177, 424);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(0, 0, 0, 0));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{55, 278, 0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, 1.0, 1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		JPanel panelQQInfo = new JPanel();
		panelQQInfo.setBorder(new EmptyBorder(0, 0, 0, 0));
		panelQQInfo.setLayout(null);
		GridBagConstraints gbc_panelQQInfo = new GridBagConstraints();
		gbc_panelQQInfo.insets = new Insets(0, 0, 5, 0);
		gbc_panelQQInfo.fill = GridBagConstraints.BOTH;
		gbc_panelQQInfo.gridx = 0;
		gbc_panelQQInfo.gridy = 0;
		contentPane.add(panelQQInfo, gbc_panelQQInfo);
		
		JLabel lblUserFace = new JLabel("");
		//判断是否不为空
		if (null!=this.message) {
			lblUserFace.setIcon(new ImageIcon("img/"+this.message.parseUserFace()));
		}
		
		lblUserFace.setBounds(0, 5, 60, 57);
		panelQQInfo.add(lblUserFace);
		
		JTextPane textPaneUserName = new JTextPane();
		textPaneUserName.setEditable(false);
		
		if (null!=this.message) {
			textPaneUserName.setText(this.message.parseUserName());
		}
		
		textPaneUserName.setBackground(Color.CYAN);
		textPaneUserName.setBounds(64, 5, 87, 57);
		panelQQInfo.add(textPaneUserName);
		
		table = new JTable();
		GridBagConstraints gbc_table = new GridBagConstraints();
		gbc_table.insets = new Insets(0, 0, 5, 0);
		gbc_table.fill = GridBagConstraints.BOTH;
		gbc_table.gridx = 0;
		gbc_table.gridy = 1;
		contentPane.add(table, gbc_table);
		
		JPanel panelButtonGroup = new JPanel();
		GridBagConstraints gbc_panelButtonGroup = new GridBagConstraints();
		gbc_panelButtonGroup.fill = GridBagConstraints.BOTH;
		gbc_panelButtonGroup.gridx = 0;
		gbc_panelButtonGroup.gridy = 2;
		contentPane.add(panelButtonGroup, gbc_panelButtonGroup);
		panelButtonGroup.setLayout(new GridLayout(1, 0, 0, 0));
		
		JButton btnFind = new JButton("");//查找
		btnFind.setBorder(new EmptyBorder(0, 0, 0, 0));//去掉四周的边距
		btnFind.setIcon(new ImageIcon("img/find.png"));
		btnFind.setHorizontalAlignment(SwingConstants.LEFT);
		
		panelButtonGroup.add(btnFind);
		
		JButton btnConfig = new JButton("");
		btnConfig.setBorder(new EmptyBorder(0, 0, 0, 0));//去掉四周的边距
		btnConfig.setIcon(new ImageIcon("img/config"));
		panelButtonGroup.add(btnConfig);
		
		JButton btnQuit = new JButton("退出");
		btnQuit.setBorder(new EmptyBorder(0, 0, 0, 0));//去掉四周的边距
		panelButtonGroup.add(btnQuit);
		
	}
	
	/**
	 * Create the frame.
	 */
	public QQListFrame() {
		this(null);
	}

}
