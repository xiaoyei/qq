package com.csxh.qq.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingWorker;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;

import com.csxh.qq.bean.QQMessage;
import com.csxh.qq.bean.QQMessageType;
import com.csxh.qq.net.Net;
import com.csxh.qq.util.StringUtil;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class QQDialog extends JDialog {

	private ImageIcon bg=new ImageIcon("img/background.png");
	private ImageIcon top_down_arrow=new ImageIcon("img/top_down_arrow.png");
	private ImageIcon top_line=new ImageIcon("img/top_line.png");
	private ImageIcon top_close=new ImageIcon("img/top_close.png");

	private ImageIcon login=new ImageIcon("img/login.png");
	
	

	private Rectangle top_down_arrow_rect=new Rectangle(340, 4, this.top_down_arrow.getIconWidth(), this.top_down_arrow.getIconHeight());
	private Rectangle top_line_rect=new Rectangle(370, 4, this.top_line.getIconWidth(), this.top_line.getIconHeight());
	private Rectangle top_close_rect=new Rectangle(400, 4, this.top_close.getIconWidth(), this.top_close.getIconHeight());
	private JTextField textFieldPassword;
	private ImageIcon keyboard_icon;
	private JComboBox<String> comboBoxQQ;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			QQDialog dialog = new QQDialog();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public QQDialog() {
		setUndecorated(true);
		setBounds(100, 100, 432, 334);
		setLocationRelativeTo(null);//对话框居中显示
		getContentPane().setLayout(new BorderLayout());
		
		JPanel backgroudImage = new JPanel(){
			
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			
			@Override
			protected void paintComponent(Graphics g) {
				// TODO Auto-generated method stub
				super.paintComponent(g);
				g.drawImage(QQDialog.this.bg.getImage(), 0, 0, null);
				//arrow=340,4
				g.drawImage(QQDialog.this.top_down_arrow.getImage(), top_down_arrow_rect.x,top_down_arrow_rect.y,top_down_arrow_rect.width,top_down_arrow_rect.height, null);
				//line 371,4
				g.drawImage(QQDialog.this.top_line.getImage(), top_line_rect.x,top_line_rect.y,top_line_rect.width,top_line_rect.height, null);
				//close 400,4
				g.drawImage(QQDialog.this.top_close.getImage(), top_close_rect.x,top_close_rect.y,top_close_rect.width,top_close_rect.height, null);
			}
			
		};//end JPanel
		
		backgroudImage.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {
				
				//获取mouse的点击位置
				if (QQDialog.this.top_close_rect.contains(e.getX(), e.getY())) {
				
					QQDialog.this.dispose();
					
				}
			}
		});
		
		getContentPane().add(backgroudImage, BorderLayout.CENTER);
		backgroudImage.setLayout(null);
		
		comboBoxQQ = new JComboBox<String>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 1084817787767531702L;

			@Override
			protected void paintComponent(Graphics g) {
				
				super.paintComponent(g);
			//	g.drawImage(QQDialog.this.login.getImage(), 0, 0, null);
			}
			
		};
		
		comboBoxQQ.addItem("123456");//调试时使用
		
		comboBoxQQ.setEditable(true);
		comboBoxQQ.setBackground(SystemColor.WHITE);
		comboBoxQQ.setBounds(135, 200, 194, 30+2);
		comboBoxQQ.setBorder(new Border() {
			
			@Override
			public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
				// TODO Auto-generated method stub
				g.setColor(new Color(209, 209, 209));
				g.drawLine(x, y, width, height);
			}
			
			@Override
			public boolean isBorderOpaque() {
				// TODO Auto-generated method stub
				return false;
			}
			
			@Override
			public Insets getBorderInsets(Component c) {
				// TODO Auto-generated method stub
				return new Insets(1, 1, 1, 1);
			}
		});
		backgroudImage.add(comboBoxQQ);
		
		JPanel panel = new JPanel();
		panel.setBorder(new EmptyBorder(1, 1, 1, 1));
		panel.setBackground(Color.WHITE);
		panel.setBounds(135, 240-10, 194, 30);
		backgroudImage.add(panel);
		panel.setLayout(null);
		
		JLabel lblKeyboardIcon = new JLabel("");
		keyboard_icon = new ImageIcon("img/keyboard.png");
		lblKeyboardIcon.setIcon(keyboard_icon);
		lblKeyboardIcon.setBounds(166, 0, 28, 30);
		panel.add(lblKeyboardIcon);
		
		textFieldPassword = new JTextField();
		textFieldPassword.setBounds(0, 0, 167, 30);
		panel.add(textFieldPassword);
		textFieldPassword.setColumns(10);
		
		textFieldPassword.setText("1234");//调试中使用
		
		JButton btnLogin = new JButton("");
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				SwingWorker<QQMessage, String>worker=new SwingWorker<QQMessage, String>(){

					@Override
					protected QQMessage doInBackground() throws Exception {
						// TODO Auto-generated method stub
						boolean b=false;
//可以通过中间执行结果加入进度条
						
						// 连接到指定IP和port的服务程序，并通信请求
						Socket client = new Socket(Net.host, Net.port);
						
						//向服务器发出QQ注册请求
						//将QQ通信协议封装在一个类QQMessage中
						String qq=QQDialog.this.comboBoxQQ.getSelectedItem().toString();
						String password=QQDialog.this.textFieldPassword.getText();
						//实际开发中密码要进行加密处理
					    QQMessage message=new QQMessage();
						//设置消息类型是注册
					    message.setType(QQMessageType.Login);
					    message.setFrom(qq);//设置QQ号
					    message.formatPassword(password);//设置密码
					    
					    String params=message.formatParams();//将属性格式为key=value&key=value;
					    
						String url=Net.buildRequestUrl(Net.host, Net.port, Net.qqMessage, params);
						
						Net.sendRequestUrl(client, url);
						
						//等待服务回答
						String data= Net.receiveResponseData(client);
						params=data;
						
						//关闭socket
						client.close();
												
						b=message.parseParams(params);
						
						return message;
						
					}//end doInBackground

					@Override
					protected void done() {
						// TODO Auto-generated method stub
						try {
							final QQMessage message=get();
							if(message.getStatus()==200){
								//登录成功，
								//先关闭QQDialog窗口;
								QQDialog.this.dispose();
								//开启另一个窗口QQListDialog
								EventQueue.invokeLater(new Runnable() {
									public void run() {
										try {
											//将QQ消息传窗口进行处理
											QQListFrame frame = new QQListFrame(message);
											frame.setVisible(true);
										} catch (Exception e) {
											e.printStackTrace();
										}
									}
								});
							}else{
								JOptionPane.showMessageDialog(null, "登录失败");
							}
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (ExecutionException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}//end doen
					
				};//end worker
				
				worker.execute();
			}
		});
		btnLogin.setIcon(new ImageIcon("img/login.png"));
		btnLogin.setBounds(138, 291, 194, 30);
		backgroudImage.add(btnLogin);
		
		JLabel lblReg = new JLabel("用户注册");
		lblReg.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				
				
				//在此完成用户的注册功能
				SwingWorker<Boolean, String>worker=new SwingWorker<Boolean, String>() {
					
					@Override
					protected Boolean doInBackground() throws Exception {
						Boolean b=true;
						
						//可以通过中间执行结果加入进度条
										

						// 连接到指定IP和port的服务程序，并通信请求
						Socket client = new Socket(Net.host, Net.port);
						
						//向服务器发出QQ注册请求
						//将QQ通信协议封装在一个类QQMessage中
						String qq=QQDialog.this.comboBoxQQ.getSelectedItem().toString();
						String password=QQDialog.this.textFieldPassword.getText();
						//实际开发中密码要进行加密处理
					    QQMessage message=new QQMessage();
						//设置消息类型是注册
					    message.setType(QQMessageType.Reg);
					    message.setFrom(qq);//设置QQ号
					    message.formatPassword(password);//设置密码
					    message.formatUserName(StringUtil.randomAlphabetString(4, 6));//随机产生一个，应该从用户控件中获取
					    message.formatUserFace("face1.jpg");//指定一个，应该从用户控件中获取
				
					    
					    String params=message.formatParams();//将属性格式为key=value&key=value;
					    
						String url=Net.buildRequestUrl(Net.host, Net.port, Net.qqMessage, params);

						//JOptionPane.showMessageDialog(null, url);
						
						Net.sendRequestUrl(client, url);
						
						//等待服务回答
						String data= Net.receiveResponseData(client);
						params=data;
						
						//关闭socket
						client.close();
												
						b=message.parseParams(params);
						
						return b && message.getStatus()==200;
					}//end doInBackground
					
					@Override
					protected void done() {
						Boolean b=false;
						try {
							b=(Boolean)this.get();
						} catch (Exception e) {
							b=false;
						}
						if (b) {
							//注册成功
							JOptionPane.showMessageDialog(QQDialog.this, "注册成功，请开始登录");
							//其它处理
						}else{
							//注册失败
							JOptionPane.showMessageDialog(QQDialog.this, "注册失败，请重新注册");
							//其它处理							
						}
						
					}//end done
					
				};//end worker
				
				worker.execute();
				
			}
		});
		lblReg.setForeground(SystemColor.textHighlight);
		lblReg.setBounds(339, 200, 59, 20);
		backgroudImage.add(lblReg);
		
		JLabel lblFindPassword = new JLabel("找回密码");
		lblFindPassword.setForeground(SystemColor.textHighlight);
		lblFindPassword.setBounds(339, 230, 59, 20);
		backgroudImage.add(lblFindPassword);
		
	}
}
