package com.csxh.qq.ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.csxh.qq.net.Net;
import com.csxh.qq.net.NetRequest;
import com.csxh.qq.net.NetResponse;
import com.csxh.qq.net.NetServer;
import com.csxh.qq.net.Servlet;
import com.csxh.qq.net.ServletConfig;

import java.awt.GridLayout;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingWorker;

import java.awt.event.ActionListener;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.awt.event.ActionEvent;

public class ServerWindow extends JDialog {

	private final JScrollPane contentScrollPane = new JScrollPane();
	private JTextArea textAreaMessage;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			ServerWindow dialog = new ServerWindow();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public ServerWindow() {
		setTitle("server window");
		setBounds(100, 100, 450, 300);
		getContentPane().setLayout(new BorderLayout());
		contentScrollPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentScrollPane, BorderLayout.CENTER);
		{
			textAreaMessage = new JTextArea();
			contentScrollPane.setViewportView(textAreaMessage);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton startButton = new JButton("Start");
				startButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						//开启后台线程
						SwingWorker<String, String> worker=new SwingWorker<String, String>(){

							@Override
							protected String doInBackground() throws Exception {
								String ret="start";
								
								ServerSocket server = new ServerSocket(Net.port);
								
								ServletConfig config=new ServletConfig();

								//发UI线程发送中间结果
								this.publish("服务器在端口监听（值班）....");
							
								while (true) {
									
									Socket client = server.accept();// 在指定端口上等待客户的到来，如果有访问到达，就返回代表客户的Socket对象
									//创建一个处理客户请求的线程对象，并启动
									NetRequest request = new NetRequest(client);
									NetResponse response = new NetResponse(client);
									//解析请求的命令（Action）与参数
									this.publish("解析参数");
									request.parse();
									
									//获取servlet的全限定名：带包的类名称
									String servletName=request.getServletName(config);
									
									this.publish("交给"+servletName+"进行处理");
									
									//根据命令来派发servlet
									//通过反射根据全限定名称来创建servlet对象
									
									
									try {
									
										Class<?> clazz= Class.forName(servletName);

										Servlet servlet=(Servlet)clazz.getConstructor(request.getClass(),response.getClass(),config.getClass()).newInstance(request,response,config);
									   	
										new Thread(servlet).start();
									
									} catch (Exception e1) {
										this.publish("无法创建servlet,"+e1.getMessage());
									}
									   	
									try{
										Thread.sleep(100);
									}catch(InterruptedException e){
										break;
									}
									
									
								}//end while
								
								server.close();
								
								
								return ret;
							}//end doInBaracground
							
							@Override
							protected void process(List<String> messageList) {
								//在UI线程中显示中间结果
								for (String message : messageList) {
									
									ServerWindow.this.textAreaMessage.append(message);
									ServerWindow.this.textAreaMessage.append("\r\n");
									
								}
								
							}
							
						};//end worker
						
						worker.execute();
					}
				});
				startButton.setActionCommand("start");
				buttonPane.add(startButton);
				getRootPane().setDefaultButton(startButton);
			}
			{
				JButton stopButton = new JButton("Stop");
				stopButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						
					}
				});
				stopButton.setActionCommand("stop");
				buttonPane.add(stopButton);
			}
		}
	}

}
