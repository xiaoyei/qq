package com.csxh.qq.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MapUtil {

	/**
	 * 增一对多的映射
	 * @param map
	 * @param key
	 * @param values
	 * @return
	 */
	public static <K,V> Map<K, List<V> > add(Map<K,  List<V>  >map, K key,V... values){
		
		List<V>valueList=  map.get(key);
		if (null==valueList) {
			valueList=new ArrayList<>();
		}
		
		for (V value : valueList) {
			
			if (!valueList.contains(value)) {
				valueList.add(value);
			}
			
		}
		
		map.put(key, valueList);
		
		return map;
	}
}
