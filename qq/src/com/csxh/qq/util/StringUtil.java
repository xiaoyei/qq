
package com.csxh.qq.util;

import java.util.Arrays;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 用于操作字符串的类
 * 
 */
public class StringUtil {

	/**
	 * 将首字符转换成大写
	 * 
	 * @param s
	 * @return
	 */
	public static String toUpperFirstChar(String s) {

		char ch = s.charAt(0);

		if (ch >= 'a' && ch <= 'z') {

			char[] chs = s.toCharArray();
			chs[0] -= 32;

			s = String.valueOf(chs);

		}

		return s;

	}

	/**
	 * 将首字符转换成小写
	 * 
	 * @param s
	 * @return
	 */
	public static String toLowerFirstChar(String s) {

		char ch = s.charAt(0);

		if (ch >= 'A' && ch <= 'Z') {

			char[] chs = s.toCharArray();
			chs[0] += 32;

			s = String.valueOf(chs);

		}

		return s;
	}

	/**
	 * 功能：获取随机的英文字符数组共len个
	 * 
	 * @param len
	 *            数字的长度（即个数）
	 * @return
	 */
	public static char[] randomDigits(int len) {

		char[] rets = new char[len];

		char[] chs = new char[10];
		int i = 0;
		for (char ch = '0'; ch <= '9'; ch++) {
			chs[i++] = ch;
		}

		for (int j = 0; j < len; j++) {
			int index = MathUtil.random(0, chs.length - 1);
			rets[j] = chs[index];
		}

		return rets;

	}

	/**
	 * 功能：获取随机的英文字符数组共len个
	 * 
	 * @param len
	 *            数字的长度（即个数）
	 * @return
	 */
	public static char[] randomAlphabets(int len) {

		char[] rets = new char[len];

		char[] chs = new char[2 * 26];
		int i = 0;
		for (char ch = 'a'; ch <= 'z'; ch++) {
			chs[i++] = ch;
		}
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			chs[i++] = ch;
		}

		for (int j = 0; j < len; j++) {
			int index = MathUtil.random(0, chs.length - 1);
			rets[j] = chs[index];
		}

		return rets;

	}
	/**
	 * 功能：获取随机的由[m,n]个字母组成的英文字符串
	 * 
	 * @param m,n 最小与最大个数
	 * @return
	 */
	public static String randomAlphabetString(int m,int n) {//m<n
		
		char[] chs = new char[2 * 26];
		int i = 0;
		for (char ch = 'a'; ch <= 'z'; ch++) {
			chs[i++] = ch;
		}
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			chs[i++] = ch;
		}
		
		StringBuilder sb=new StringBuilder();
		
		int len=MathUtil.random(m, n);
		for (int j = 0; j < len; j++) {
			int index=MathUtil.random(0, chs.length-1);
			sb.append(chs[index]);
		}
		
		return sb.toString();
		
	}

	/**
	 * 功能：获取随机的英文与数字字符数组共len个
	 * 
	 * @param len
	 *            数字的长度（即个数）
	 * @return
	 */
	public static char[] randomAlphabetAndDigits(int len) {

		char[] rets = new char[len];

		char[] chs = new char[2 * 26 + 10];
		int i = 0;

		for (char ch = '0'; ch <= '9'; ch++) {
			chs[i++] = ch;
		}

		for (char ch = 'a'; ch <= 'z'; ch++) {
			chs[i++] = ch;
		}
		for (char ch = 'A'; ch <= 'Z'; ch++) {
			chs[i++] = ch;
		}

		for (int j = 0; j < len; j++) {
			int index = MathUtil.random(0, chs.length - 1);
			rets[j] = chs[index];
		}

		return rets;

	}

	/**
	 * 求一个串中元素的全排列数组
	 * 
	 * @param elems
	 *            "abcd"
	 * @return
	 */
	public static void permuate(String elems,String[] rets) {

		// 回归的条件
		if (elems == null || "".equals(elems)) {
			return ;
		}
		// 取第一个元素
		char ch = elems.charAt(0);
		// 取第1个开始的所有字符组成的子串
		String sub = elems.substring(1);
		String[] rs={};
		permuate(sub,rs);// 获取除第1个字符之外的其余子串的全排
		
		StringBuilder sb = new StringBuilder();// 带缓存的字符串
		for (int i = 0; i < rs.length; i++) {
		
			String s=Arrays.toString(rs);
			// 将第一个元素分别插入子串的每一个位置
			for (int j = 0; j < s.length(); j++) {
                     
				    sb.setLength(0);
				    sb.append(s);// 带缓存的字符串
					sb = sb.insert(j, ch);// 获取一个全排
					rets = ArrayUtil.add(sb.toString(), rets);			
			}
	
		}
		
		return ;

	}
	/**
	 * 判断是否为一个有交t的点分十进制IPV4地址串 [000,255].xx.xx.xx,0.0.0.0
	 * @param input
	 * @return
	 */
	public static boolean isIPV4(String input){
		
		String regex="^([0-9])|([0-9][0-9])|([01][0-9][0-9])|([2][0-4][0-9])|([2][5][0-5])\\.([0-9])|([0-9][0-9])|([01][0-9][0-9])|([2][0-4][0-9])|([2][5][0-5])\\.([0-9])|([0-9][0-9])|([01][0-9][0-9])|([2][0-4][0-9])|([2][5][0-5])\\.([0-9])|([0-9][0-9])|([01][0-9][0-9])|([2][0-4][0-9])|([2][5][0-5])$";
		return Pattern.compile(regex).matcher(input).find();
		
	}
	
	/**
	 * 判断是否为一个身份证：18位
	 * @param input
	 * @param suffixs 号码后缀可选参数，如com,cn,net,
	 * @return
	 */
	public static boolean isIdCards(String input){
		String regex="^\\d{17}[\\dx]$";
		return input.matches(regex);
		
	}
	/**
	 * 判断是否为一个邮件地址：xxx@yyy.(com|cn|dnd)
	 * @param input
	 * @param suffixs 号码后缀可选参数，如com,cn,net,
	 * @return
	 */
	public static boolean isEmails(String input,String... suffixs){

		String regex;
		if (suffixs.length==0) {
			regex="^[0-9a-zA-z_]+@[0-9a-zA-z_]+\\.\\w+$";
		}else{
			regex="^[0-9a-zA-z_]+@[0-9a-zA-z_]+\\.";
			regex+="(";
			for (int i = 0; i < suffixs.length; i++) {
				regex+=suffixs[i];
				if (i<suffixs.length-1) {
				   regex+="|";	
				}
			}
			regex+=")$";

		}
		
		return input.matches(regex);
		
		
	}
	/**
	 * 判断是否为一个正确的电话号码
	 * @param input
	 * @param prefixs 号码前缀可选参数，如137,150,,
	 * @return
	 */
	public static boolean isPhoneNumbers(String input,String... prefixs){
		
		String regex="^";
		
		if (prefixs.length>0) {
			regex+="(";
		}
		for (int i = 0; i < prefixs.length; i++) {
			regex+=prefixs[i];
			if (i<prefixs.length-1) {
			   regex+="|";	
			}
		}
		if (prefixs.length>0) {
			regex+=")";
		}
		
		if (prefixs.length==0) {			
			regex+="[0-9]{11}$";
		}else{
			regex+="[0-9]{8}$";			
		}
		
		return input.matches(regex);
		
	}
	
	/**
	 * 检测输入字符串是否符合格式要求
	 * @param input
	 * @param min 最小长度
	 * @param max 最大长度
	 * @param regexs 必须包含的字符的正则表达
	 * @return true or false
	 */
	public static boolean checkFormat(String input,int min,int max,String... regexs){
		
		if (input.length()<min || input.length()>max) {
			return false;
		}
		
		for (String regex : regexs) {
			if (!input.matches(".*"+regex+".*")) {
				return false;
			}
		}
		
		return true;
	}
	
	/**
	 * 裁剪输入串左右两边的指定字符集
	 * @param input
	 * @param chars 被裁剪的字符数组
	 * @return
	 */
	public static boolean trimChars(String input,char[] chars){
		
		return false;
	}
	
	/**
	 * 裁剪输入串左右两边符合正则表达式的字符
	 * @param input
	 * @param regexs 正则表达式，可以有多个，如果没有，则表达裁剪空格
	 * @return 被裁剪后的新字符串
	 */
	public static String trim(String input,String... regexs){
		
		if (regexs.length==0) {
			return input.trim();
		}
		
		String regex="(";
		for (int i = 0; i < regexs.length; i++) {
			regex+=regexs[i];
			if (i<regexs.length-1) {
				regex+="|";//加入选择符号
			}
		}
		regex+=")";
		
		String pattern="(?<=^"+regex+")(?<input>.*)(?="+regex+"$)";
		Pattern p=Pattern.compile(pattern);
		
		Matcher m=p.matcher(input);
		if (m.find()) {
			input=m.group("input");
			
			while (true) {
				//判断结果串是否还存在与模式相匹配的内容
				m = p.matcher(input);
				if (m.find()) {//存在就取形成新的结果串
					input = m.group("input");
				} else{//不存在就退出
					break;
				}
			}
		}
		
		return input;
	}

	/**
	 * 判断字符串是否为空
	 * @param s
	 * @return
	 */
	public static boolean isEmpty(String s) {
		if (s==null || "".equals(s)) {
			return true;
		}
		return false;
	}
	/**
	 * 判断字符串是否不为空
	 * @param s
	 * @return
	 */
	public static boolean isNotEmpty(String s) {
		if (s!=null || !"".equals(s)) {
			return true;
		}
		return false;
	}

	
	/**
	 * 寻找指定的左右匹配串对
	 * @param input "[xxxx]"
	 * @param leftRegex :左侧模式 
	 * @param rightRegex :右侧模式 
	 * @param start :左侧模式符出现的首字符索引号，由它传到方法外部的变量
	 * @param end :右侧模式出现的首字符索引号，由它传到方法外部的变量
	 * @param modes true=懒惰式匹配，从左向右匹配，false=饥饿式匹配，从两端向中间匹配，默认为饥饿式匹配
	 * @return true or false
	 */
	public static boolean findMatchers(String input,String leftRegex,String rightRegex, Integer start,Integer end, boolean... modes){
		
		start=end=-1;
		
		String regex="";
		
		boolean mode=modes.length>0 ? modes[0] : false;
		
		if (mode) {//懒惰式
				regex="(?<="+leftRegex+")(.*?)(?="+rightRegex+")";//0,1
		}else{
			regex="(?<="+leftRegex+")(.*)(?="+rightRegex+")";//0,1			
		}
		
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(input);
		
		if (m.find()) {
			start=m.start(1)-1;
			end=m.end(1)+1;
			return true;
		}
		
		return false;
	}

	/**
	 * 寻找指定的左右匹配串对
	 * @param input "[xxxx]"
	 * @param leftRegex :左侧模式 
	 * @param rightRegex :右侧模式 
	 * @param modes true=懒惰式匹配，从左向右匹配，false=饥饿式匹配，从两端向中间匹配，默认为饥饿式匹配
	 * @return true or false
	 */
	public static boolean findMatchers(String input,String leftRegex,String rightRegex, boolean... modes){
		
		Integer start=0,end=0;
		boolean mode=modes.length>0 ? modes[0] : false;		
		return findMatchers(input, leftRegex, rightRegex,start,end, mode);
	}
	/**
	 * 获取左右匹配串对之间的子串
	 * @param input:<h1>xxx</h2>
	 * @param leftRegex :左侧模式 
	 * @param rightRegex :右侧模式 
	 * @param modes true=懒惰式匹配，从左向右匹配，false=饥饿式匹配，从两端向中间匹配，默认为饥饿式匹配
	 * @return 子串
	 */
	public static String subMatchers(String input,String leftRegex,String rightRegex, boolean... modes){
		
		Integer start=0,end=0;
		 
		boolean mode=modes.length>0 ? modes[0] : false;
		if (findMatchers(input, leftRegex,rightRegex, start, end, mode)) {
			return input.substring(start+1, end);
		}else{
			return input;
		}
		
	}
	
	/**
	 * 替换匹配符之间的子串
	 * @param input
	 * @param matchers : "[]","<>" "{}" "()" "''" 等
	 * @param replacement:替换的内容
	 * @return 被替换后的新串
	 */
	public static String replaceMatchers(String input,String leftRegex,String rightRegex,String replacement){
		
		
		
		return "";
	}
	
	
	/**
	 * 寻找指定的匹配字符对
	 * @param input "[xxxx]"
	 * @param matchers : "[]","<>" "{}" "()" "''" 等
	 * @param start :匹配符出现的开始索引号，由它传到方法外部的变量
	 * @param end :匹配符出现的结束索引号，由它传到方法外部的变量
	 * @param modes true=懒惰式匹配，从左向右匹配，false=饥饿式匹配，从两端向中间匹配
	 * @return true or false
	 */
	public static boolean findMatchers(String input,String matchers,Integer start,Integer end, boolean... modes){
		
		start=end=-1;
		
		if(isEmpty(matchers)){
			return false;
		}
		
		char ch=matchers.charAt(0);
		String left=String.valueOf(ch);
		switch(ch){
		case '|':
		case '(':
		case ')':
		case '{':
		case '}':
		case ']':
		case '[':
		case '<':
		case '>':
		case '=':
			left="\\"+left;
			break;
		}
		ch=matchers.charAt(matchers.length()-1);
		String right=String.valueOf(ch);
		switch(ch){
		case '|':
		case '(':
		case ')':
		case '{':
		case '}':
		case ']':
		case '[':
		case '<':
		case '>':
		case '=':
			right="\\"+right;
			break;
		}
		
		boolean mode = modes.length>0 ? modes[0] : false;
		
		return findMatchers(input, left, right, mode);
	}

	/**
	 * 寻找指定的匹配字符对
	 * @param input "[xxxx]"
	 * @param matchers : "[]","<>" "{}" "()" "''" 等
	 * @param modes true=懒惰式匹配，从左向右匹配，false=饥饿式匹配，从两端向中间匹配
	 * @return true or false
	 */
	public static boolean findMatchers(String input,String matchers,boolean...modes){
		
		Integer start=-1,end=input.length();
		boolean mode = modes.length>0 ? modes[0] : false;
		return findMatchers(input, matchers, start, end, mode);
		
	}

	
	/**
	 * 获取匹配符之间的子串
	 * @param input:xx{ddddd}yy
	 * @param matchers : "[]","<>" "{}" "()" "''" 等
	 * @return 子串
	 */
	public static String subMatchers(String input,String matchers){
		
		Integer start=0,end=0;
		if (findMatchers(input, matchers, start, end, true)) {
			return input.substring(start+1, end);
		}else{
			return input;
		}
		
	}
	
	/**
	 * 替换匹配符之间的子串
	 * @param input
	 * @param matchers : "[]","<>" "{}" "()" "''" 等
	 * @param replacement:替换的内容
	 * @return 被替换后的新串
	 */
	public static String replaceMatchers(String input,String matchers,String replacement){
		
		
		return "";
	}
	
	
	
	
}