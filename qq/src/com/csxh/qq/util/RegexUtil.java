package com.csxh.qq.util;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式工具类
 * 
 * @author hp
 *
 */
public class RegexUtil {

	public final static String ProtocolFormat = "^(%s)?(://)?(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*((:\\d+)?)(/(\\w+(-\\w+)*))*(\\.?(\\w)*)(\\?)?(((\\w*%%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*(\\w*%%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*)*(\\w*)*)$";

	public final static String UrlPath = "UrlPath";

	public static Map<String, Object> parseProtocol(String name, String url) {

		Map<String, Object> map = new HashMap<>();

		Pattern p = Pattern.compile(String.format(ProtocolFormat, name), Pattern.CASE_INSENSITIVE);
		Matcher m = p.matcher(url.trim());
		if (m.find()) {

			map.put(UrlPath, m.group(10));

			String params = m.group(16);

			if (!StringUtil.isEmpty(params)) {
				// k1=v1 &k2=v2
				String[] ss = params.split("&");
				for (int i = 0; i < ss.length; i++) {
					String[] ss2 = ss[i].split("=");
					String key = ss2[0].trim();
					String value = ss2[1].trim();
					// 判断是否可以转换为数值
					try {
						Integer v = Integer.valueOf(value);
						map.put(key, v);
					} catch (NumberFormatException e) {
						try {
							Double f = Double.valueOf(value);
							map.put(key, f);
						} catch (NumberFormatException e1) {

							if (value.equalsIgnoreCase("true")) {
								map.put(key, true);
							} else if (value.equalsIgnoreCase("false")) {
								map.put(key, false);
							} else {
								map.put(key, value);
							}

						}
					}
				}
			}

		} else {
			throw new RuntimeException("协议格式不正确");
		}

		return map;

	}

	public static String parseProtocolUrlPath(String name, String url) {

		Map<String, Object> map = parseProtocol(name, url);

		String urlPath = (String) map.get(UrlPath);

		return urlPath;
	}

	public static void main(String[] args) {

		String regex = "http://([\\w-]+\\.)+[\\w-]+(/[\\w- ./?%&=]*)?";

		Pattern p = Pattern.compile(regex);
		String url = "qq://xxxx.yyy.com/xxx/yyy?k1=v1&k2=true";

		

	//	String lastPart = RegexUtil.parseProtocolUrlPath("http", url);
	//	Map<String, Object> params = RegexUtil.parseProtocol("http", url);

	//	System.out.println(lastPart);
	//	System.out.println(params);

		String regexFormat = "^(%s)?(://)?(\\w+(-\\w+)*)(\\.(\\w+(-\\w+)*))*((:\\d+)?)(/(\\w+(-\\w+)*))*(\\.?(\\w)*)(\\?)?(((\\w*%%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*(\\w*%%)*(\\w*\\?)*(\\w*:)*(\\w*\\+)*(\\w*\\.)*(\\w*&)*(\\w*-)*(\\w*=)*)*(\\w*)*)$";

	//	p = Pattern.compile(String.format(regexFormat, "jdbc"), Pattern.CASE_INSENSITIVE);

		regex = "^(?<name>qq)(://)(?<host>\\w+(-\\w+)*(\\.\\w+(-\\w+)*)*)(?<port>:[\\d]+)?/(?<path>\\w+(/\\w+)*)*(?<params>\\?(\\w+=\\w+[\\.\\w+]*)(&\\w+=\\w+[\\.\\w+]*)*)*$";
		p=Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
		
		url="qq://www.localhost.com:8888/downloadFile?filename=f1.txt";
			
		Matcher m = p.matcher(url);
		if (m.find()) {
			for (int i = 0; i < m.groupCount(); i++) {
				System.out.print("" + i + "-->");
				System.out.println(m.group(i));
			}

			System.out.println("path="+m.group("path"));
			System.out.println("params="+m.group("params"));
		}

		
		/*
		 * String lowerAlpha = "[a-z]"; String upperAlpha = "[A-Z]"; String
		 * alpha = String.format("(%s|%s)", lowerAlpha, upperAlpha); String
		 * digit = "[0-9]"; String safe = "(\\$|-|_|\\.|\\+)"; String extra =
		 * "(!|\\*|'|\\(|\\)|,)"; String hex =
		 * String.format("(%s|A|B|C|D|E|F|a|b|c|d|e|f)", digit); String escape =
		 * String.format("(%%s%s)", hex,hex,hex); String unreserved =
		 * String.format("(%s|%s|%s|{3})", alpha, digit, safe, extra); String
		 * uchar = String.format("(%s|%s)", unreserved, escape); String reserved
		 * = "(;|/|\\?|:||&|=)"; String xchar = String.format("(%s|%s|%s)",
		 * unreserved, reserved, escape); String digits = String.format("(%s+)",
		 * digit);
		 * 
		 * String alphadigit = String.format("(%s|%s)", alpha, digit); String
		 * domainlabel = String.format("(%s|%s(%s|-)*%s)", alphadigit); String
		 * toplabel = String.format("(%s|%s(%s|-)*%s)", alpha, alphadigit);
		 * String hostname = String.format("((%s\\.)*%s)", domainlabel,
		 * toplabel); String hostnumber = String.format("%s\\.%s\\.%s\\.%s",
		 * digits); String host = String.format("(%s|%s)", hostname,
		 * hostnumber); String port = digits; String hostport =
		 * String.format("(%s(:%s){{0,1}})", host, port); String hsegment =
		 * String.format("((%s|;|:||&|=)*)", uchar); String search =
		 * String.format("((%s|;|:||&|=)*)", uchar); String hpath =
		 * String.format("%s(/%s)*", hsegment); String httpurl =
		 * String.format("http://%s(/%s(\\?%s){{0,1}}){{0,1}}", hostport, hpath,
		 * search);
		 */

	}

}
