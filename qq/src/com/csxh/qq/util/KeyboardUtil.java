package com.csxh.qq.util;

import java.util.Scanner;

/*
* 用于获取键盘输入的类
*/
public class KeyboardUtil {

	// 定义一个扫描器对象（类中的变量）
	private static Scanner scr = null;
	// 定义扫描到的整型值变量
	private static int intValue = Integer.MIN_VALUE;
	private static String strValue = "";
	private static int[] intArray=new int[]{};
	
	/**
	 * xxx 功能：判断键盘是否有下一行输入 参数：title=提示文本
	 * 返回：true=有输入，false=无输入
	 */
	public static boolean hasNextLine(String title) {

		if (null == KeyboardUtil.scr) {
			KeyboardUtil.scr = new Scanner(System.in);
		}

		// 打印输入提示
		System.out.print(title);

		KeyboardUtil.strValue = scr.nextLine();

		return true;

	}

	/*
	 * 功能：获取键盘的输入一行 参数：无 返回值：输入的String值
	 */
	public static String nextLine() {

		return KeyboardUtil.strValue;
	}

	/*
	 * 功能：判断键盘是否有下一个输入 参数：title-->用户的输入提示，errorMessage--->发生错误的信息
	 */
	public static boolean hasNextInt(String title, String... errorMessages) {
        
		if (null == KeyboardUtil.scr) {
			KeyboardUtil.scr = new Scanner(System.in);
		}

		// 打印输入提示
		System.out.print(title);
		try {
			// 保存输入的整数值
			KeyboardUtil.intValue = scr.nextInt();

		} catch (Exception e) {

			System.out.println(errorMessages.length==0 ? e.getMessage() : errorMessages[0]);
		
			scr = null;// 清除扫描器，以便下一次重新创建

			return false;
		}

		return true;

	}

	/*
	 * 功能：获取键盘的输入int值 参数：无 返回值：输入的int值
	 */
	public static int nextInt() {

		return KeyboardUtil.intValue;
	}

	/**
	 * 判断是否输入了以逗号或空格分隔的多个整数
	 * 
	 * @param count
	 *            TODO
	 * @param title
	 * @param errorMessage
	 * 
	 * @return
	 */
	public static boolean hasNextInts(int count, String title, String... errorMessages) {
		// 1、输入一行数字值串，以逗号或空格
		boolean b = hasNextLine(title);
		if (!b) {
			return false;
		}
		// m,n
		String range = nextLine();
		// 除去左右空格
		range = range.trim();
		String[] ss = range.split("[\\s,]+");// 以空格或逗号进行分割
		if (ss.length != count) {
			return false;
		}
		//解析每一个输入的数字串
		KeyboardUtil.intArray=new int[0];
		
		try {

			for (int i = 0; i < ss.length; i++) {
				
				int v = Integer.parseInt(ss[i]);
				KeyboardUtil.intArray=ArrayUtil.add(v, KeyboardUtil.intArray);
				
			}

		} catch (NumberFormatException e) {
			
			System.out.println(errorMessages.length==0 ? e.getMessage() : errorMessages[0]);
			return false;
		}

		return true;

	}
	
	public static int[] nextInts(){
		return KeyboardUtil.intArray;
	}
	

}