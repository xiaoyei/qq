package com.csxh.qq.util;

public class CharacterUtil {

	public static boolean isVowels(char ch){
		
		return ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u' || ch=='A' || ch=='E' || ch=='I' || ch=='O' || ch=='U'; 
		
	}

}
